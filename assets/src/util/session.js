export const login = (user) =>
  fetch("/api/sessions", {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
  });

export const signup = (user) =>
  fetch("/api/users/register", {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
  });

export const logout = () =>
  fetch("/api/sessions", {
    method: "DELETE",
  });

export const checkLoggedIn = async () => {
  try {
    const response = await fetch("/api/sessions");
    const { session } = await response.json();

    return { session };
  } catch (e) {
    console.log(e);
  }
};
