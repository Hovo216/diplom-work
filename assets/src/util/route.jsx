import React from "react";
import { connect } from "react-redux";
import { Redirect, Route, withRouter } from "react-router-dom";
import { compose } from "redux";

const mapStateToProps = (state) => {
  return {
    loggedIn: Boolean(state.session.user && !!state.session.user.uuid),
    user: state.session.user,
  };
};

const Auth = ({
  loggedIn,
  path,
  component: Component,
  redirect_path,
  user,
  props,
}) => {
  return (
    <Route
      path={path}
      render={() => {
        return loggedIn ? (
          <Redirect to={user !== undefined ? redirect_path : "/"} />
        ) : (
          <Component {...props} />
        );
      }}
    />
  );
};

export const AuthRoute = compose(
  withRouter,
  connect(mapStateToProps, {})
)(Auth);
