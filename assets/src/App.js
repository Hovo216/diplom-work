import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";

import NavbarContainer from "./components/Navbar/container";
import HomePageContainer from "./components/Home Page/container";
import LoginContainer from "./components/Login/container";
import TrainingsContainer from "./components/Trainings/container";
import NutritionContainer from "./components/Nutritions/container";
import AboutContainer from "./components/About Us/container";

import "./App.css";

function App(props) {
  return (
    <div
      className={"mx-auto containerBig"}
      style={{
        minHeight: "700px",
      }}
    >
      <NavbarContainer />
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/home" />} />
        <Route exact path="/home" render={() => <HomePageContainer />} />
        <Route path="/login" component={LoginContainer} />
        <Route path="/trainings" component={TrainingsContainer} />
        <Route path="/nutritions" component={NutritionContainer} />
        <Route path="/about" component={AboutContainer} />
      </Switch>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    state: state,
  };
};

export default connect(mapStateToProps, {})(App);
