export const createUser = async (attrs) => {
  try {
    const response = await fetch("http://localhost:3004/api/users/register", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(attrs),
    });

    if (response.status === 400) {
      return {
        message: "User with this email allready exists.",
        data: [],
      };
    }

    if (response.status === 500) {
      return {
        message: "Connection error.",
        data: [],
      };
    }

    if (response.status === 201) {
      return {
        message: "none",
        data: await response.json(),
      };
    }
  } catch (err) {
    return {
      message: "error",
    };
  }
};

export const authUser = async (attrs) => {
  try {
    const response = await fetch("http://localhost:3004/api/sessions", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(attrs),
    });

    if (response.status === 400) {
      return {
        message: "Wrong credentials.",
        data: [],
      };
    }

    if (response.status === 404) {
      return {
        message: "User is not found",
        data: [],
      };
    }

    if (response.status === 500) {
      return {
        message: "Connection error.",
        data: [],
      };
    }

    if (response.status === 201) {
      return {
        message: "none",
        data: await response.json(),
      };
    }
  } catch (err) {
    return {
      message: "error",
    };
  }
};
