import {
  CREATE_USER,
  SET_ERROR_MSG_REG,
  SET_ERROR_MSG_AUTH,
} from "../constants/user";

const initState = {
  errorMessageAuth: "",
  errorMessageReg: "",
};

export default (state = initState, action) => {
  switch (action.type) {
    case CREATE_USER: {
      return {
        ...state,
        user: action.user,
      };
    }
    case SET_ERROR_MSG_REG: {
      return {
        ...state,
        errorMessageReg: action.message,
      };
    }
    case SET_ERROR_MSG_AUTH: {
      return {
        ...state,
        errorMessageAuth: action.message,
      };
    }
    default:
      return state;
  }
};
