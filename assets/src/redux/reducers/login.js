import { CHANGE_LOGIN_FORM } from "../constants/login";

const initState = {
  login_form: "",
};

export default (state = initState, action) => {
  switch (action.type) {
    case CHANGE_LOGIN_FORM: {
      return {
        ...state,
        login_form: action.hash,
      };
    }
    default:
      return state;
  }
};
