import { combineReducers } from "redux";

import user from "./user";
import login from "./login";
import session from "./session";
import trainings from "./trainings";

export default combineReducers({ user, login, session, trainings });
