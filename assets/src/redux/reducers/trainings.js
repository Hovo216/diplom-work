const initState = {
  videos: [
    {
      src: "https://www.youtube.com/embed/xqBTEdJfHE4",
      name: "Full-Body Circuit Workout",
      desription:
        "This quickie circuit from Brandan Fokken will have you in and out of the gym fast. If you only go to the gym 2-3 times per week, Fokken recommends doing this workout every session. If you're a more frequent gym-goer, throw it in once a week or every other week.",
    },
    {
      src: "https://www.youtube.com/embed/qfMjEOLQarA",
      name: "Upper Body Strength | Full-Body Muscle-Building Home Workouts",
      desription:
        "These full-body workouts utilize exercises that can be performed with nothing more than a dumbbell or kettlebell and a band—and you'll get suggested substitutions even if you don't have those. You'll use techniques like 1.5 reps, compound sets, chip-away sets, and timed sets to get the most intensity out of each of your three weekly workouts, getting great pumps and feeling the good hurt.",
    },
    {
      src: "https://www.youtube.com/embed/Wn7Kr7dCtH8",
      name: "Strong Bands for Strong Legs",
      desription:
        "Get into a split-squat stance, with a 90-degree bend in the front and back knees, and anchor the band under the arch of your front foot. If you put the band under your toe, you'll unweight that part of the foot when you pull up and lose your balance. Reach down and grab each side of the band with your hands.",
    },
    {
      src: "https://www.youtube.com/embed/0tegWZWnDtE",
      name: "20-Minute Upper Abs Workout | The 7 Day Six Pack",
      desription:
        "The major benefit of an equipment-free ab workout is that you can do it anywhere. The downside is that it's far more difficult to increase the overload without weights. But if this is the best fit for you, don't let that obstacle scare you away!",
    },
    {
      src: "https://www.youtube.com/embed/Ofq2lRzusYE",
      name: "Explosive Workout Routine for Athletes",
      desription:
        "Definitely give this workout from Raynor Whitcombe a try if you're looking to help improve your strength, conditioning, and explosiveness in your specific sport. ",
    },
    {
      src: "https://www.youtube.com/embed/kPAhwzZYG2g",
      name: "Build Your Upper Chest w/ Hypertrophy",
      desription:
        "Considering that the definition of hypertrophy is an increase in the size of skeletal muscle, growing bigger muscles is an obvious by-product of high-volume training. The more muscle you have, the greater your strength potential, and adding that muscle requires dedicated hypertrophy phases, even if it means swallowing your pride and taking occasional breaks from your max-out days.",
    },
    {
      src: "https://www.youtube.com/embed/swmDoMfQDpo",
      name: "High-Intensity Workout for Fat Loss",
      desription:
        "Get moving and transform your body with simple and effective functional exercises you can do anytime, anywhere. This simple yet effective six-week program, designed by Australian strength and fitness coach Vince Ozalp, is your first step to a healthier, better you!",
    },
    {
      src: "https://www.youtube.com/embed/ZtlH0A5dlLg",
      name: "5 Best Exercises For A Bigger Chest",
      desription:
        "Building the chest of your dreams shouldn't be treated like rocket science—overly complicated with a chance of blowing up in your face. James Grage likes to stick to the basics, using different angles and rep ranges to blitz every fiber in his pecs and elicit maximum growth.",
    },
  ],
};

export default (state = initState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
