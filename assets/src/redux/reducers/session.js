import {
  RECEIVE_CURRENT_USER,
  LOGOUT_CURRENT_USER,
  GET_ERROR,
} from "../constants/session";

const _nullSession = {
  user: {
    email: "",
    uuid: "",
  },
  errorLogin: null,
};

export default (state = _nullSession, action) => {
  switch (action.type) {
    case RECEIVE_CURRENT_USER: {
      return {
        ...state,
        ...action.user,
      };
    }
    case LOGOUT_CURRENT_USER:
      return {
        ..._nullSession,
      };
    case GET_ERROR: {
      return {
        ...state,
        errorLogin: action.error,
      };
    }
    default:
      return state;
  }
};
