import {
  CREATE_USER,
  SET_ERROR_MSG_REG,
  SET_ERROR_MSG_AUTH,
} from "../constants/user";

import * as API from "../API/user-api";

export const craeteUser = (user) => ({
  type: CREATE_USER,
  user,
});

export const setErrorMsg = (message) => ({
  type: SET_ERROR_MSG_REG,
  message,
});

export const setErrorMsgAuth = (message) => ({
  type: SET_ERROR_MSG_AUTH,
  message,
});

export const create_user = (attrs) => async (dispatch) => {
  const response = await API.createUser(attrs);
  console.log(response.message);
  dispatch(setErrorMsg(response.message));
};

export const auth_user = (attrs) => async (dispatch) => {
  const response = await API.authUser(attrs);
  dispatch(setErrorMsgAuth(response.message));
};
