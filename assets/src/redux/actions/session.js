import * as apiUtil from "../../util/session";

import {
  RECEIVE_CURRENT_USER,
  LOGOUT_CURRENT_USER,
  GET_ERROR,
} from "../constants/session";

const receiveCurrentUser = (user) => ({
  type: RECEIVE_CURRENT_USER,
  user,
});

const logoutCurrentUser = () => ({
  type: LOGOUT_CURRENT_USER,
});

export const getError = (error) => ({ type: GET_ERROR, error });

export const login = (user) => async (dispatch) => {
  try {
    const response = await apiUtil.login(user);
    const data = await response.json();

    if (data.bool) {
      dispatch(getError(null));
      return dispatch(receiveCurrentUser(data.session));
    }

    return dispatch(getError("wrongLogin"));
  } catch (err) {
    return dispatch(getError("wrongLogin"));
  }
};

export const logout = () => async (dispatch) => {
  try {
    const response = await apiUtil.logout();

    if (response.ok) {
      return dispatch(logoutCurrentUser());
    }

    return;
  } catch (err) {
    console.log(err);
    return { status: "error", message: err };
  }
};
