import { CHANGE_LOGIN_FORM } from "../constants/login";

export const changeLoginForm = (hash) => ({
  type: CHANGE_LOGIN_FORM,
  hash,
});
