import React from "react";

import HomePage from "./view";

const HomePageContainer = (props) => {
  return <HomePage />;
};

export default HomePageContainer;
