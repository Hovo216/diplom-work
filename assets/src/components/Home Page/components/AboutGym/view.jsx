import React, { useState } from "react";

import Styles from "./styles.module.css";

const AboutGym = (props) => {
  const [viewBtn, setViewBtn] = useState("false");

  const viewBtnClick = () => {
    setViewBtn(!viewBtn);
  };

  return (
    <div className={Styles.container}>
      <div className={Styles.innerContainer}>
        <h1 className={Styles.header}>About Gym</h1>
        <h5 className={Styles.underHead}>SPORTZONE HEALTH & RACQUET CLUB</h5>
        <div className={Styles.orangeLine}></div>
        <p className={Styles.textAboutGym}>
          We’ve transformed every aspect of our membership to encompass
          integrated digital and in-club offerings to keep you at your best—all
          the time, any time.
        </p>
        <div className={Styles.orangeArea}></div>
        <div className={Styles.cardsArea}>
          <div style={{ margin: "-15px" }}>
            <div
              className={Styles.layout}
              style={{ height: viewBtn ? "400px" : "1230px" }}
            >
              <div className={`${Styles.item} ${Styles.item_1}`}></div>
              <div className={`${Styles.item} ${Styles.item_2}`}></div>
              <div className={`${Styles.item} ${Styles.item_3}`}></div>
              <div className={`${Styles.item} ${Styles.item_4}`}></div>
              <div className={`${Styles.item} ${Styles.item_5}`}></div>
              <div className={`${Styles.item} ${Styles.item_6}`}></div>
              <div className={`${Styles.item} ${Styles.item_7}`}></div>
              <div className={`${Styles.item} ${Styles.item_8}`}></div>
            </div>
          </div>
        </div>
        <button className={Styles.viewMore} onClick={() => viewBtnClick()}>
          {viewBtn ? "Show More" : "Show less"}
        </button>
      </div>
    </div>
  );
};

export default AboutGym;
