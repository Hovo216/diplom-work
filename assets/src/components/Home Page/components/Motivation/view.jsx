import React from "react";

import Styles from "./styles.module.css";
import motiv_1 from "../../../../static/images/motiv-1.jpg";
import motiv_2 from "../../../../static/images/motiv-2.jpg";

const Motivation = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.layout}>
        <div className={Styles.motiv_1_content}>
          <img src={motiv_1} alt="motivation" />
        </div>
        <div className={Styles.motiv_2}>
          <h5 className={Styles.motiv_head}>Increase motivation</h5>
          <p className={Styles.motiv_text}>
            Contact or visit us at one of our health and wellness facilities to
            make the best move of your life. Discover the perks of membership as
            you get fit, stay fit and focus on your health and wellness.
          </p>
          <img src={motiv_2} alt="motivation" />
        </div>
      </div>
    </div>
  );
};

export default Motivation;
