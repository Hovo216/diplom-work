import React from "react";

import GymInformation from "./view";

const GymInformationContainer = (props) => {
  return <GymInformation />;
};

export default GymInformationContainer;
