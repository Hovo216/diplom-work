import React from "react";

import Styles from "./styles.module.css";
import item_1_svg from "../../../../static/images/dumbbell.svg";
import item_2_svg from "../../../../static/images/waist.svg";
import item_3_svg from "../../../../static/images/team.svg";
import item_4_svg from "../../../../static/images/weight.svg";

const GymImformation = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.innerContainer}>
        <div className={Styles.cardsContent}>
          <div className={Styles.card}>
            <div className={Styles.cardContainer}>
              <div className={Styles.iconRound}>
                <img src={item_1_svg} alt="item_1" />
              </div>
              <h5 className={Styles.cardHead}>About Gym</h5>
              <p className={Styles.cardText}>
                With the help of our certified trainers, you would be able to
                become stronger and healthy.
              </p>
            </div>
          </div>
          <div className={Styles.card}>
            <div className={Styles.cardContainer}>
              <div className={Styles.iconRound}>
                <img src={item_2_svg} alt="item_2" />
              </div>
              <h5 className={Styles.cardHead}>Lose Fat</h5>
              <p className={Styles.cardText}>
                With our cardio training and diets from our nutritionists, you
                would lose weight and be healthy.
              </p>
            </div>
          </div>
          <div className={Styles.card}>
            <div className={Styles.cardContainer}>
              <div className={Styles.iconRound}>
                <img src={item_3_svg} alt="item_3" />
              </div>
              <h5 className={Styles.cardHead}>Training</h5>
              <p className={Styles.cardText}>
                Our certified instructors create a personalized training program
                for you to be healthy.
              </p>
            </div>
          </div>
          <div className={Styles.card}>
            <div className={Styles.cardContainer}>
              <div className={Styles.iconRound}>
                <img src={item_4_svg} alt="item_4" />
              </div>
              <h5 className={Styles.cardHead}>Our Team</h5>
              <p className={Styles.cardText}>
                Our friendly and helpful team will motivate you to reach your
                fitness goals and refuse all illnesses.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GymImformation;
