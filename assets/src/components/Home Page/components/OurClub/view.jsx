import React from "react";

import Styles from "./styles.module.css";

const OurClub = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.layout}>
        <h1>Our sports club is open!</h1>
        <p>
          Join today by paying our $39 processing and $49 initiation fees and
          get the rest of the year FREE!
        </p>
      </div>
    </div>
  );
};

export default OurClub;
