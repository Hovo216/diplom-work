import React from "react";

import Styles from "./styles.module.css";

import facebook from "../../../../static/images/facebook.svg";
import twitter from "../../../../static/images/twitter.svg";
import instagram from "../../../../static/images/instagram.svg";
import youtube from "../../../../static/images/youtube.svg";

import membership from "../../../../static/images/membership.jpg";

const Membership = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.layout}>
        <div className={Styles.layoutRow}>
          <div className={Styles.member_1}>
            <h5>membership</h5>
            <h2>Membership Benefits</h2>
            <p>
              With membership in our sport club, you will have an
              apportunityWith a membership in our sports club, you will have the
              opportunity to use the services of our nutritionist, get an
              individual diet and exercise program, have your own locker and of
              course be one of us.
            </p>
            <div
              className={`${Styles.socialLinks} d-flex justify-content-between`}
            >
              <a href="https://www.facebook.com/" className={Styles.links}>
                <img src={facebook} alt="fb" />
              </a>
              <a href="https://twitter.com/" className={Styles.links}>
                <img src={twitter} alt="tw" />
              </a>
              <a href="https://www.instagram.com/" className={Styles.links}>
                <img src={instagram} alt="inst" />
              </a>
              <a href="https://www.youtube.com/" className={Styles.links}>
                <img src={youtube} alt="yt" />
              </a>
            </div>
          </div>
          <div className={Styles.member_2}>
            <h4>You’ll discover how easy it is to stay motivated and fit</h4>
          </div>
          <div className={Styles.member_3}>
            <img src={membership} alt="membership" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Membership;
