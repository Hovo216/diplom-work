import React from "react";
import { NavLink } from "react-router-dom";

import Styles from "./styles.module.css";

const Header = (props) => {
  return (
    <div className={Styles.container}>
      <div className={`${Styles.textArea} col`}>
        <h4 className="row">
          We are committed to helping you exceed your fitness goals. With our
          indoor pool, basketball courts, group exercise classes and
          state-of-the-art fitness floor, you will find exactly what you are
          looking for.
        </h4>
        <NavLink to="/about">
          <button className={`${Styles.joinBtn} row`}>JOIN NOW</button>
        </NavLink>
      </div>
      <div className={`${Styles.imgArea} d-flex align-items-end`}>
        <h1>Sport Zone</h1>
      </div>
    </div>
  );
};

export default Header;
