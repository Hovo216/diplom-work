import React from "react";

import Styles from "./styles.module.css";

import HeaderContainer from "./components/Header/container";
import AboutGymContainer from "./components/AboutGym/container";
import GymInformationContainer from "./components/Information/container";
import MotivationContainer from "./components/Motivation/container";
import OurClubContainer from "./components/OurClub/container";
import MembershipContainer from "./components/Membership/container";
import Footer from "../Footer/view";

const HomePage = (props) => {
  return (
    <div className={Styles.container}>
      <HeaderContainer />
      <AboutGymContainer />
      <GymInformationContainer />
      <MotivationContainer />
      <OurClubContainer />
      <MembershipContainer />
      <Footer />
    </div>
  );
};

export default HomePage;
