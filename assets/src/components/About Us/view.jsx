import React from "react";

import Styles from "./styles.module.css";
import Footer from "../Footer/view";

const About = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.header}>
        <div className={Styles.gradient}></div>
        <h1 className={Styles.title}>Contact Us</h1>
      </div>
      <div className={Styles.infoAbout}>
        <div className={Styles.innerAbout}>
          <h2>You can Find Us!</h2>
          <div className={Styles.contentAbout}>
            <div className={Styles.geolocation}>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1830.8493797260962!2d44.524072396302046!3d40.79741353566114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4041bbe8116cffd3%3A0x7b3461102da5ad34!2sVanadzor%20Technological%20Center!5e0!3m2!1sen!2s!4v1618752833708!5m2!1sen!2s"
                width="500"
                height="350"
                style={{ border: 0 }}
                loading="lazy"
                title="map"
              ></iframe>
            </div>
            <div className={Styles.contactInfo}>
              <label htmlFor="address">Address</label>
              <p id="address">12 Shinararneri St, Vanadzor</p>
              <label htmlFor="contactEmail">Email</label>
              <p id="contactEmail">sport_zone@gmail.com</p>
              <label htmlFor="phone">Tel.</label>
              <p id="phone">+37477889955</p>
              <p id="phone">+37498889955</p>
            </div>
          </div>
        </div>
      </div>
      <div className={Styles.contactArea}>
        <div className={Styles.orangeArea}></div>
        <div className={Styles.contactForm}>
          <h3>Contact Us</h3>
          <form className={`${Styles.form} col`}>
            <div className={Styles.inputGroup}>
              <label htmlFor="name" className={Styles.formLabel}>
                Name
              </label>
              <input type="text" id="name" name="name" placeholder="Name" />
            </div>

            <div className={Styles.inputGroup}>
              <label htmlFor="surname" className={Styles.formLabel}>
                Surname
              </label>
              <input
                type="text"
                id="surname"
                name="surname"
                placeholder="Surname"
              />
            </div>
            <div className={Styles.inputGroup}>
              <label htmlFor="contact" className={Styles.formLabel}>
                Contact
              </label>
              <input
                type="text"
                id="contact"
                name="contact"
                placeholder="Enter your phone number"
              />
            </div>
            <div className={Styles.inputGroup}>
              <label htmlFor="date" className={Styles.formLabel}>
                Date
              </label>
              <input type="date" id="date" name="date" />
            </div>
            <div className={Styles.inputGroupLarge}>
              <label htmlFor="contact" className={Styles.formLabel}>
                Message
              </label>
              <textarea
                id="message"
                name="message"
                placeholder="Enter your message"
                rows="5"
                cols="50"
              ></textarea>
              <button>Submit</button>
            </div>
          </form>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default About;
