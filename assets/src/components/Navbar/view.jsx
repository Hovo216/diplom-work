import React from "react";
import { NavLink } from "react-router-dom";

import logo from "../../static/images/navbar_logo.png";
import Styles from "./styles.module.css";

const Navbar = (props) => {
  return (
    <div className={`${Styles.container}`}>
      <div className="container">
        <nav
          className="navbar navbar-expand-lg "
          style={{ paddingTop: "0.3rem", paddingBottom: 0 }}
        >
          <div className="container-fluid">
            <a className="navbar-brand" href="/">
              <img className={Styles.navbarLogo} src={logo} alt="logo" />
            </a>
            <span className={Styles.brand}>Sport Zone</span>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className={`${Styles.nav_item} nav-item`}>
                <NavLink
                  to={"/home"}
                  activeClassName={Styles.nav_item_active}
                  className={"nav-link"}
                >
                  Home
                </NavLink>
              </li>
              <li className={`${Styles.nav_item} nav-item`}>
                <NavLink
                  to={"/trainings"}
                  activeClassName={Styles.nav_item_active}
                  className={"nav-link"}
                >
                  Trainings
                </NavLink>
              </li>
              <li className={`${Styles.nav_item} nav-item`}>
                <NavLink
                  to={"/nutritions"}
                  activeClassName={Styles.nav_item_active}
                  className={"nav-link"}
                >
                  Nutritions
                </NavLink>
              </li>
              <li className={`${Styles.nav_item} nav-item`}>
                <NavLink
                  to={"/about"}
                  activeClassName={Styles.nav_item_active}
                  className={"nav-link"}
                >
                  About Us
                </NavLink>
              </li>
            </ul>
          </div>
          {!props.session_user || props.session_user.email === "" ? (
            <div
              className={`${Styles.buttons_container} d-flex justify-content-between`}
            >
              <NavLink to="/login/signin">
                <button className={Styles.user_button_log}>Log In</button>
              </NavLink>
              <NavLink to="/login/signup">
                <button className={Styles.user_button_sign}>Sign Up</button>
              </NavLink>
            </div>
          ) : (
            <div
              className={`${Styles.buttons_container} d-flex align-items-center justify-content-between`}
            >
              <h6>{props.session_user.email}</h6>
              <button
                className={Styles.user_button_logout}
                onClick={props.logout}
              ></button>
            </div>
          )}
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
