import React from "react";
import { connect } from "react-redux";
import Navbar from "./view";

import { logout } from "../../redux/actions/session";

const NavbarContainer = (props) => {
  const logout = (e) => {
    e.preventDefault();
    props.logout();
  };
  return <Navbar session_user={props.session_user} logout={logout} />;
};

const mapStateToProps = (state) => {
  return {
    session_user: state.session.user,
  };
};

export default connect(mapStateToProps, {
  logout,
})(NavbarContainer);
