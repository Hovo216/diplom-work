import React from "react";

import Styles from "./styles.module.css";

import facebook from "../../static/images/facebook_color.svg";
import twitter from "../../static/images/twitter_color.svg";
import instagram from "../../static/images/instagram_color.svg";
import youtube from "../../static/images/youtube_color.svg";

const Footer = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.innerFooter}>
        <div className={Styles.footerText}>
          <h4>&#169; Sport Zone Fitness</h4>
          <p>All Rights reserved</p>
        </div>
        <div className={`${Styles.socialLinks} d-flex justify-content-between`}>
          <a href="https://www.facebook.com/" className={Styles.links}>
            <img src={facebook} alt="fb" />
          </a>
          <a href="https://twitter.com/" className={Styles.links}>
            <img src={twitter} alt="tw" />
          </a>
          <a href="https://www.instagram.com/" className={Styles.links}>
            <img src={instagram} alt="inst" />
          </a>
          <a href="https://www.youtube.com/" className={Styles.links}>
            <img src={youtube} alt="yt" />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
