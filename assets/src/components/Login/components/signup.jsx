import React from "react";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";

import Styles from "../styles.module.css";

const SignUp = (props) => {
  const signupSchema = Yup.object().shape({
    firstName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("*First name is required"),
    lastName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("*Last name is required"),
    email: Yup.string().email("*Invalid email").required("*Email is required"),
    password: Yup.string()
      .min(6, "Too Short!")
      .max(12, "Too Long!")
      .required("*Password is required"),
    confirmPass: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  return (
    <div style={{ marginTop: "20px" }}>
      {props.errorMessage !== "none" && props.errorMessage !== "" ? (
        <div className={Styles.error_msg}>{props.errorMessage}</div>
      ) : null}
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          confirmPass: "",
        }}
        onSubmit={(values) => {
          props.createUser(values);
        }}
        validationSchema={signupSchema}
      >
        {({ errors, touched }) => (
          <Form className={Styles.form}>
            {errors.firstName && touched.firstName ? (
              <div className={Styles.error_msg}>{errors.firstName}</div>
            ) : null}
            <Field
              id="firstName"
              name="firstName"
              placeholder="*First Name"
              className={Styles.field}
            />
            {errors.lastName && touched.lastName ? (
              <div className={Styles.error_msg}>{errors.lastName}</div>
            ) : null}
            <Field
              id="lastName"
              name="lastName"
              placeholder="*Last Name"
              className={Styles.field}
            />
            {errors.email && touched.email ? (
              <div className={Styles.error_msg}>{errors.email}</div>
            ) : null}
            <Field
              id="email"
              name="email"
              type="email"
              placeholder="*Email"
              className={Styles.field}
            />
            {errors.password && touched.password ? (
              <div className={Styles.error_msg}>{errors.password}</div>
            ) : null}
            <Field
              id="password"
              type="password"
              name="password"
              placeholder="*Password"
              className={Styles.field}
            />
            {errors.confirmPass && touched.confirmPass ? (
              <div className={Styles.error_msg}>{errors.confirmPass}</div>
            ) : null}
            <Field
              id="confirmPass"
              name="confirmPass"
              type="password"
              placeholder="*Confirm Password"
              className={Styles.field}
            />
            <div className={Styles.userButtons}>
              <a href="/login/signin" className={Styles.tabLink}>
                I have an account
              </a>
              <button type="submit" className={Styles.logButton}>
                sign up
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default SignUp;
