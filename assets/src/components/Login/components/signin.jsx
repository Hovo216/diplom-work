import React from "react";
import { Form, Formik, Field } from "formik";
import * as Yup from "yup";

import Styles from "../styles.module.css";

const SignIn = (props) => {
  const signinSchema = Yup.object().shape({
    email: Yup.string().email("*Invalid email").required("*Email is required"),
    password: Yup.string()
      .min(6, "Too Short!")
      .max(12, "Too Long!")
      .required("*Password is required"),
  });

  return (
    <div style={{ marginTop: "20px" }}>
      {props.errorMessageAuth !== "none" && props.errorMessageAuth !== "" ? (
        <div className={Styles.error_msg}>{props.errorMessageAuth}</div>
      ) : null}
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={(values) => {
          props.authUser(values);
        }}
        validationSchema={signinSchema}
      >
        {({ errors, touched }) => (
          <Form className={Styles.form}>
            {errors.email && touched.email ? (
              <div className={Styles.error_msg}>{errors.email}</div>
            ) : null}
            <Field
              id="email"
              name="email"
              type="email"
              placeholder="*Email"
              className={Styles.field}
            />
            {errors.password && touched.password ? (
              <div className={Styles.error_msg}>{errors.password}</div>
            ) : null}
            <Field
              id="password"
              name="password"
              placeholder="*Password"
              type="password"
              className={Styles.field}
            />
            <div className={Styles.userButtons}>
              <a href="/login/signup" className={Styles.tabLink}>
                Create an account
              </a>
              <button type="submit" className={Styles.logButton}>
                sign in
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default SignIn;
