import React, { useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";

import Login from "./view";

import { changeLoginForm } from "../../redux/actions/login";

import { create_user, auth_user } from "../../redux/actions/user";

import { login, logout } from "../../redux/actions/session";

const LoginContainer = (props) => {
  const createUser = (attrs) => {
    props.create_user(attrs);
  };

  const authUser = (attrs) => {
    props.login(attrs);
  };

  useEffect(() => {
    if (props.errorMessageReg === "none") {
      return (window.location.href = "/login/signin");
    }
  }, [props.errorMessageReg]);

  useEffect(() => {
    if (props.errorMessageAuth === "none") {
      return (window.location.href = "/home");
    }
  }, [props.errorMessageAuth]);

  return (
    <Login
      login_form={props.login_form}
      changeLoginForm={props.changeLoginForm}
      createUser={createUser}
      authUser={authUser}
      errorMessageReg={props.errorMessageReg}
      errorMessageAuth={props.errorLogin}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    login_form: state.login.login_form,
    errorMessageReg: state.user.errorMessageReg,
    errorMessageAuth: state.user.errorMessageAuth,
    errorLogin: state.session.errorLogin,
  };
};

export default compose(
  connect(mapStateToProps, {
    changeLoginForm,
    create_user,
    auth_user,
    login,
    logout,
  })
)(LoginContainer);
