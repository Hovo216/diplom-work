import React from "react";

import Styles from "./styles.module.css";
import { Route, Switch } from "react-router-dom";
import { AuthRoute } from "../../util/route";

import SignIn from "./components/signin";
import SignUp from "./components/signup";

const Login = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.layout}>
        <div className={Styles.gradient}>
          <div className={Styles.auth}>
            <div className={Styles.formTable}>
              <div className={Styles.logoSite}></div>
              <h1 className={Styles.head}>Sport Zone</h1>
              <h4 className={Styles.slogan}>
                Join to our team and become stronger with us
              </h4>
              <div style={{ marginTop: "20px" }}>
                <Switch>
                  <AuthRoute
                    path="/login/signin"
                    props={{
                      authUser: props.authUser,
                      errorMessageAuth: props.errorMessageAuth,
                    }}
                    component={SignIn}
                    redirect_path="/home"
                  />
                  <AuthRoute
                    path="/login/signup"
                    props={{
                      createUser: props.createUser,
                      errorMessage: props.errorMessageReg,
                    }}
                    component={SignUp}
                    redirect_path="/login/signin"
                  />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
