import React from "react";

import Styles from "./styles.module.css";

import carbs from "../../static/images/carbs.jpg";
import water from "../../static/images/water.jpg";
import protein from "../../static/images/proteins.jpg";

import Footer from "../Footer/view";

const Nutrition = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.header}>
        <div className={Styles.gradient}></div>
        <h1 className={Styles.title}>Nutritions</h1>
      </div>
      <div className={Styles.info_card}>
        The experts at SportZone have compiled hundreds of articles, FAQs,
        videos, podcasts and more, all to help you learn as much as you want
        about any fitness or nutrition topic, from the basics to fully
        referenced articles
      </div>
      <div className={Styles.gradient_line}></div>
      <div className={Styles.nutrition}>
        <div className={Styles.innerCont}>
          <div className={Styles.textGroupNutrition}>
            <div className={Styles.textGroupHead}>
              <hr />
              <span className={Styles.nutroIcon}></span>
              <hr />
            </div>
            <h3>Nutrition</h3>
            <p>
              Proper nutrition fuels your workouts and helps you get the most
              out of your time at the club. That’s why our trainers work with
              you to develop customized nutrition plans through dotFIT that
              focus on your specific needs, tastes, and dietary restrictions.
            </p>
            <p>
              Your plan comes with an amazing online platform to keep you
              feeling awesome about your weight loss, strength training or
              rehabilitation journey. With personalized coaching and a diverse
              community of supportive peers, our partnership with dotFIT makes
              the hard work of working out just a little easier. HIITZone and
              Crunch Signature Personal Trainers are dotFIT experts ready to
              help you get healthy!
            </p>
            <button className={Styles.joinBtn} onClick={() => props.openLink()}>
              Learn More
            </button>
          </div>
          <div className={Styles.personal_group}></div>
        </div>
      </div>
      <hr className={Styles.nutritionLine} />
      <div className={`${Styles.nutrition_receips} col`}>
        <div className={`${Styles.header_nutro} row`}>
          <div className={Styles.nutroGroupHead}>
            <hr />
            <h1>Nutrition</h1>
            <hr />
          </div>
          <p>
            Nutrition is the study of nutrients in food, how the body uses them,
            and the relationship between diet, health, and disease. Nutrition
            also focuses on how people can use dietary choices to reduce the
            risk of disease, what happens if a person has too much or too little
            of a nutrient, and how allergies work.
          </p>
        </div>
      </div>
      <div className={`${Styles.nutroCard} row`}>
        <div
          className={Styles.nutroImg}
          style={{ backgroundImage: `url(${carbs})` }}
        ></div>
        <div className={Styles.nutroText}>
          <h4>Carbohydrates</h4>
          <p>Sugar, starch, and fiber are types of carbohydrates.</p>
          <p>
            Sugars are simple carbs. The body quickly breaks down and absorbs
            sugars and processed starch. They can provide rapid energy, but they
            do not leave a person feeling full. They can also cause a spike in
            blood sugar levels. Frequent sugar spikes increase the risk of type
            2 diabetes and its complications.
          </p>
          <p>
            Fiber is also a carbohydrate. The body breaks down some types of
            fiber and uses them for energ; others are metabolized by gut
            bacteria, while other types pass through the body.
          </p>
          <p>
            Fiber and unprocessed starch are complex carbs. It takes the body
            some time to break down and absorb complex carbs. After eating
            fiber, a person will feel full for longer. Fiber may also reduce the
            risk of diabetes, cardiovascular disease, and colorectal cancer.
            Complex carbs are a more healthful choice than sugars and refined
            carbs.
          </p>
        </div>
      </div>
      <hr className={Styles.nutroLine} />
      <div className={`${Styles.nutroCard} row`}>
        <div
          className={Styles.nutroImg}
          style={{ backgroundImage: `url(${protein})` }}
        ></div>
        <div className={Styles.nutroText}>
          <h4>Proteins</h4>
          <p>
            Proteins consist of amino acids, which are organic compounds that
            occur naturally.
          </p>
          <p>
            There are 20 amino acids. Some of these are essentialTrusted Source,
            which means people need to obtain them from food. The body can make
            the others.
          </p>
          <p>
            Some foods provide complete protein, which means they contain all
            the essential amino acids the body needs. Other foods contain
            various combinations of amino acids.
          </p>
          <p>
            Most plant-based foods do not contain complete protein, so a person
            who follows a vegan diet needs to eat a range of foods throughout
            the day that provides the essential amino acids.
          </p>
        </div>
      </div>
      <hr className={Styles.nutroLine} />
      <div className={`${Styles.nutroCard} row`}>
        <div
          className={Styles.nutroImg}
          style={{ backgroundImage: `url(${water})` }}
        ></div>
        <div className={Styles.nutroText}>
          <h4>Water</h4>
          <p>Sugar, starch, and fiber are types of carbohydrates.</p>
          <p>
            The adult human body is up to 60% water, and it needs water for many
            processes. Water contains no calories, and it does not provide
            energy.
          </p>
          <p>
            Many people recommend consuming 2 liters, or 8 glasses, of water a
            day, but it can also come from dietary sources, such as fruit and
            vegetables. Adequate hydration will result in pale yellow urine.
          </p>
          <p>
            Requirements will also depend on an individual’s body size and age,
            environmental factors, activity levels, health status, and so on.
          </p>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Nutrition;
