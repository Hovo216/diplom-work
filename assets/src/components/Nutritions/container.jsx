import React from "react";

import Nutrition from "./view";

const NutritionContainer = (props) => {
  const openLink = () => {
    window.open(
      "https://www.medicalnewstoday.com/articles/160774#:~:text=Nutrition%20is%20the%20study%20of,nutrients%20affect%20the%20human%20body.",
      "_blank"
    );
  };
  return <Nutrition openLink={openLink} />;
};

export default NutritionContainer;
