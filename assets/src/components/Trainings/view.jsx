import React from "react";

import Styles from "./styles.module.css";

import VideoCards from "./TrainingVideos/container";

import Footer from "../Footer/view";
import { NavLink } from "react-router-dom";

const Trainings = (props) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.header}>
        <div className={Styles.gradient}></div>
        <h1 className={Styles.title}>Trainings</h1>
      </div>
      <div className={Styles.info_card}>
        Our certified personal trainers create the perfect programs to help you
        reach your goals, designed 100% around you. And they’ll be cheering you
        on when you cross that finish line like a badass, too.
      </div>
      <div className={Styles.gradient_line}></div>
      <div className={Styles.layout}>
        <div className={Styles.innerCont}>
          <div className={Styles.trainer}></div>
          <div className={Styles.textGroup}>
            <h1>It's One-on-Wonderful</h1>
            <p>
              Here at Crunch, we focus on bringing you the most powerful
              workouts to meet your needs: our toolbox includes weighted battle
              ropes, kettlebells, TRX® suspension straps, MMA equipment,
              Bulgarian Bags, and the list goes on.
            </p>
            <p>
              It may sound like a mouthful, but this world-class functional
              training equipment helps our trainers provide you with an array of
              High Intensity Interval Training (HIIT) workouts.
            </p>
            <p>
              Our Crunch Fitness locations offer HIITZone, an all-encompassing
              fitness program including personal training, small group training,
              and nutrition.
            </p>
            <p>Let’s make a lunge date!</p>
          </div>
        </div>
      </div>
      <div className={Styles.personalTrain}>
        <div className={Styles.innerCont}>
          <div className={Styles.textGroupPersonal}>
            <div className={Styles.textGroupHead}>
              <hr />
              <span className={Styles.personalIcon}></span>
              <hr />
            </div>
            <h3>Personal Training</h3>
            <p>
              You can think of personal training like our secret sauce – it’s
              the fastest and safest way to get seriously awesome results. Our
              experienced trainers develop fun (yes, fun!) individualized
              fitness programs that incorporate cutting-edge exercises and
              nutrition to help you crush your fitness goals. Plus, Crunch
              Fitness personal trainers in the HIITZone program use the
              Combative Training Center to help you reach your fitness goals.
            </p>
            <p>
              With specialized certifications that ensure you’re getting an
              effective workout, our trainers provide limitless motivation for
              everyone. You don’t need to be a professional athlete or aspiring
              fitness guru to be part of our programs. And if you’re already a
              professional athlete, we still want to hang with you.
            </p>
            <NavLink to="/about">
              <button className={Styles.joinBtn}>Join now</button>
            </NavLink>
          </div>
          <div className={Styles.personal}></div>
        </div>
      </div>
      <div className={Styles.personalTrain}>
        <div className={Styles.innerCont}>
          <div className={Styles.textGroupPersonal}>
            <div className={Styles.textGroupHead}>
              <hr />
              <span className={Styles.personalIconGroup}></span>
              <hr />
            </div>
            <h3>Small Group Training</h3>
            <p>
              Get sweaty with your best friend or make a new workout buddy on
              the turf in our group training sessions! With a team environment
              to motivate you through that last rep, your results are waiting
              just behind that punching bag. Our trainers will keep you
              challenged and engaged with expert attention on your road to
              glory.
            </p>
            <p>
              Speak to your favorite personal trainer to learn more about small
              group training sessions at your club. There are tons of fun small
              group training programs to choose from under the HIITZone
              umbrella.
            </p>
            <NavLink to="/about">
              <button className={Styles.joinBtn}>Join now</button>
            </NavLink>
          </div>
          <div className={Styles.personal_group}></div>
        </div>
      </div>
      <hr className={Styles.trainingsLine} />
      <div className={`${Styles.trainings_videos} col`}>
        <div className={`${Styles.header_video} row`}>
          <div className={Styles.videoGroupHead}>
            <hr />
            <h1>Training Videos</h1>
            <hr />
          </div>
          <p>
            Our training videos will motivate you and help to become stronger.
            You can also try this trainings with our knowledgable trainers, who
            can give you a usefull advices and help understand how to train.
          </p>
        </div>
        {!props.session_user || props.session_user.email === "" ? (
          <div className={Styles.available}>
            <p>Training Videos will be available after egistration.</p>
          </div>
        ) : (
          <VideoCards />
        )}
      </div>
      <Footer />
    </div>
  );
};

export default Trainings;
