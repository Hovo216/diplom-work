import React from "react";
import { connect } from "react-redux";

import Trainings from "./view";

const TrainingsContainer = (props) => {
  return <Trainings session_user={props.session_user} />;
};

const mapStateToProps = (state) => {
  return {
    session_user: state.session.user,
  };
};

export default connect(mapStateToProps, {})(TrainingsContainer);
