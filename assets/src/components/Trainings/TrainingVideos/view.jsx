import React from "react";

import Styles from "../styles.module.css";

const TrainingsVideosCards = (props) => {
  return (
    <div>
      <iframe
        width="390"
        height="300"
        src={props.src}
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <h4 className={Styles.trainingName}>{props.name}</h4>
      <p className={Styles.trainingDesc}>{props.description}</p>
    </div>
  );
};

export default TrainingsVideosCards;
