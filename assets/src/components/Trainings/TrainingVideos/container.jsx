import React from "react";

import TrainingVideoCard from "./view";

import Styles from "../styles.module.css";
import { connect } from "react-redux";

const TrainingVideoCardContainer = (props) => {
  const VideoCards = props.training_videos.map((x, index) => {
    return (
      <TrainingVideoCard
        src={x.src}
        name={x.name}
        description={x.desription}
        key={`${x.src}-${index}`}
      />
    );
  });

  return <div className={`${Styles.videosInner} row`}>{VideoCards}</div>;
};

const mapStateToProps = (state) => {
  return {
    training_videos: state.trainings.videos,
  };
};

export default connect(mapStateToProps, {})(TrainingVideoCardContainer);
