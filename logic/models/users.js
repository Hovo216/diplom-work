"use strict";
module.exports = (Sequelize, DataTypes) => {
  const Users = Sequelize.define(
    "Users",
    {
      uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      email: {
        type: DataTypes.STRING,
        unique: true,
      },
      first_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      last_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      password_hash: DataTypes.STRING,
      is_active: DataTypes.BOOLEAN,
    },
    {
      underscored: true,
    }
  );
  return Users;
};
