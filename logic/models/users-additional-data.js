"use strict";
module.exports = (sequelize, DataTypes) => {
  const users_additional_data = sequelize.define(
    "users_additional_data",
    {
      uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,

        allowNull: false,
        primaryKey: true,
      },
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      country: DataTypes.STRING,
      city: DataTypes.STRING,
    },
    {
      underscored: true,
    }
  );

  users_additional_data.associate = function (models) {
    users_additional_data.belongsTo(models.Users, {
      foreignKey: "user_uuid",
      as: "user",
    });
  };

  return users_additional_data;
};
