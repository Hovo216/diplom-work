const { Users } = require("../models/users");

async function create_user(attrs) {
  try {
    await Users.create(attrs);
    return { status: "ok" };
  } catch (err) {
    console.error(err);
    return { status: "error", message: "unexpected error" };
  }
}

const User = {
  create_user,
};

module.exports = User;
