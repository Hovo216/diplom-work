require("dotenv").config();

const DB_USER = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_NAME = process.env.DB_NAME;
const DB_NAME_TEST = process.env.DB_NAME_TEST;
const DB_NAME_PROD = process.env.DB_NAME_PROD;
const DB_DIALECT = process.env.DB_DIALECT;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: "127.0.0.1",
    post: 1443,
    dialect: DB_DIALECT,
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME_TEST,
    host: "127.0.0.1",
    post: 1443,
    dialect: DB_DIALECT,
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME_PROD,
    host: "127.0.0.1",
    post: 1443,
    dialect: DB_DIALECT,
  },
};
