var express = require("express");
var router = express.Router();

{
  const { create, auth } = require("./controllers/user_controller");

  router.post("/users/register", create);
  // router.post("/users/auth", auth);
}

{
  const {
    index,
    create,
    destroy,
  } = require("./controllers/session_controller");

  router.get("/sessions", index);
  router.post("/sessions", create);
  router.delete("/sessions", destroy);
}

module.exports = router;
