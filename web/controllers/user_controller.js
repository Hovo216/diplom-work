const { Users } = require("../../logic/models");

const argon2 = require("argon2");

const create = async (req, res) => {
  try {
    const {
      email,
      firstName: first_name,
      lastName: last_name,
      password,
    } = req.body;

    const password_hash = await argon2.hash(password);
    console.log("password hash", password_hash);
    const is_email_unique = await Users.findOne({
      where: {
        email,
      },
    });

    if (is_email_unique !== null) {
      return res.status(400).json({
        status: "error",
        message: "User with this email allready exists.",
        type: "EXISTS_EMAIL",
      });
    }

    const user = await Users.create({
      email,
      first_name,
      last_name,
      password_hash,
    });

    res.status(201).json(user);
  } catch (err) {
    console.error(err);

    res.status(500).json({
      type: "INTERNAL_SERVER_ERROR",
      message: "Internal server error. Please try again.",
    });
  }
};

const auth = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user_cridentials = await Users.findOne({
      where: {
        email,
      },
    });

    if (user_cridentials === null) {
      return res.status(404).json({
        status: "error",
        message: "User is not found",
        type: "USER_NOT_FOUND",
      });
    }

    if (await argon2.verify(user_cridentials.password_hash, password)) {
      res.status(201).json(user_cridentials);
    } else {
      return res.status(400).json({
        status: "error",
        message: "Wrong credentials.",
        type: "WRONG_CREDENTIALS",
      });
    }
  } catch (error) {
    console.error(error);

    res.status(500).json({
      type: "INTERNAL_SERVER_ERROR",
      message: "Internal server error. Please try again.",
    });
  }
};

module.exports = { create, auth };
