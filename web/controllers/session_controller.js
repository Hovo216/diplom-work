const { Users } = require("../../logic/models");

const argon2 = require("argon2");

const index = (req, res) => {
  return res.status(200).json({
    status: "ok",
    session: req.session,
    sessionId: req.session.id,
  });
};

const create = async (req, res) => {
  const { email, password } = req.body;
  try {
    const { uuid, password_hash } = await Users.findOne({
      where: {
        email,
      },
    });

    if (uuid === null) {
      return res.status(404).json({
        status: "error",
        message: "User is not found",
        type: "USER_NOT_FOUND",
      });
    }

    if (await argon2.verify(password_hash, password)) {
      req.session.user = {
        uuid,
        email: email.toLowerCase(),
      };
    } else {
      return res.status(400).json({
        status: "error",
        message: "Wrong credentials.",
        type: "WRONG_CREDENTIALS",
      });
    }

    res.status(201).json({ status: "ok", bool: true, session: req.session });
  } catch (err) {
    res.status(500).json({ bool: false, error: "Something went wrong" });
  }
};

const destroy = async (req, res) => {
  try {
    // const { email } = req.session.user;

    req.session.destroy((err) => {
      if (err) {
        return res.status(400).json({ status: "error", message: err.message });
      }

      res.clearCookie("sid").status(204).end();
    });
  } catch (err) {
    res.status(500).json({ status: "error", message: err.message });
  }
};

module.exports = {
  index,
  create,
  destroy,
};
