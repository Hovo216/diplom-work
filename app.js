var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
const { createProxyMiddleware } = require("http-proxy-middleware");

var api = require("./web/router");
var app = express();

app.set("trust proxy", 1);
const session = require("express-session");
const redis = require("redis");
const connectRedis = require("connect-redis");
var bodyParser = require("body-parser");
// const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// enable this if you run behind a proxy (e.g. nginx)

const RedisStore = connectRedis(session);
//Configure redis client
const redisClient = redis.createClient({
  host: "localhost",
  port: 6379,
});
redisClient.on("error", function (err) {
  console.log("Could not establish a connection with redis. " + err);
});
redisClient.on("connect", function (err) {
  console.log("Connected to redis successfully");
});
//Configure session middleware
app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: "secret$%^134",
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: false, // if true only transmit cookie over https
      httpOnly: false, // if true prevent client side JS from reading the cookie
      maxAge: 100000 * 60 * 60 * 2, // session max age in miliseconds
    },
  })
);

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const PORT = process.env.PORT;

app.set("port", PORT || "3000");

app.use(
  "/api",
  api,
  createProxyMiddleware("/api", {
    target: `localhost:${PORT}`,
    changeOrigin: true,
  })
);

app.use((req, res, next) => {
  if (req.url !== "/api/sessions" && req.method !== "POST") {
    if (Boolean(req.session.user) === false) {
      return res.json({
        status: "ok",
        session: req.session,
        sessionId: req.session.id,
      });
    }
  }

  next();
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send("error");
});

module.exports = app;
